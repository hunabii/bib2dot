#!/usr/bin/env python

# Converter from .bib to .dot
# Rafael Ferrer October 2008
# Time-stamp: <2012-03-15 09:29:42 rafaelferrerflores>

# Initial arguments
import sys

def errorsillo():
    print '\n     Usage: %s [-t|-k] <inputfile.bib>' % (sys.argv[0])
    print ' \n     Default output is -t -k'
    print '\n     -t Use this option if you want a time line-authors in the ouput\n        (without keywords)\n'
    print '     -k Use this option if you want a keywords relations in the ouput\n       (without time line-authors)\n'
    print '     -h Display this help\n'
    sys.exit(1)

if len(sys.argv) == 1 or len(sys.argv) > 3:
    errorsillo()

if len(sys.argv) == 2:
    flnm = sys.argv[1]
    if flnm.find('.bib') == -1:
        print '\n     Input file should have a .bib extension\n'
        errorsillo()
    opt_t,opt_k = (True,True)

if len(sys.argv) == 3:
    flnm = sys.argv[2]

    if flnm.find('.bib') == -1:
        print '\n     Input file should have a .bib extension\n'
        errorsillo()
    if sys.argv[1] == '-t':
        opt_t,opt_k = (True, False)
    elif sys.argv[1] == '-k':
        opt_t,opt_k = (False, True)
    elif sys.argv[1] == '-h':
        errorsillo()
    else:
        print '\n     Unknown option: %s\n' % (sys.argv[1])
        errorsillo()

## Regular expressions
import re
p_type_of_entry = re.compile('@(.*){')
p_key = re.compile('@.*{(.*),', re.IGNORECASE)
p_end_of_entry = re.compile('}}\n')
p_author = re.compile('author[ |\t]*=[ |\t]*{(.*)}[,|}]', re.IGNORECASE)
p_auauthor = re.compile('(\S*\,\s\S*)', re.IGNORECASE)
p_auauthor1 = re.compile('(.*)', re.IGNORECASE)
p_year = re.compile('year.*=.*(\d\d\d\d)', re.IGNORECASE)
p_keyword = re.compile('keywords.=.{(.*)}', re.IGNORECASE)


## Defs

def unique_items(L):
    """Returns a new list containing the unique items from L."""
    U = []
    for item in L:
        if item not in U:
            U.append(item)
    return U

def outputo():
    outpt = open(flnm.replace('.bib','.dot'),'w')
# Header
    outpt.write('digraph EmbTimb {\n')
    outpt.write('//Initial configuration ------------------------------------------------\n')
    outpt.write('	compound=true;\n')
    outpt.write('	node [label="\N"];\n')
    outpt.write(' 	graph [bb="0,0,352,238",\n')
    outpt.write(' 	       _draw_="c 5 -white C 5 -white P 4 0 0 0 238 352 238 352 0 ",\n')
    outpt.write(' 	       xdotversion="1.2"];\n')
    outpt.write('//----------------------------------------------------------------------\n')

# Long labels
    outpt.write('\n// Long labels\n')
    outpt.write('node [shape=box,fontcolor=darkblue,style=filled,fillcolor=ghostwhite];\n')
    for i in long_labs:
        outpt.write(''.join(i))

# Years
    if opt_t == True:
        outpt.write('\n// Years\n')
        outpt.write('node [fontcolor=black,shape=plaintext,style=filled,fillcolor=white];\n')
        y_readyly = y_ready + ';\n' 
        outpt.write(''.join(y_readyly))

# Long labels
    outpt.write('\n// Authors\n')
    for i in auth_yea:
        outpt.write(''.join(i))

# Relations
    if opt_k == True:
        outpt.write('\n// Relations\n')
        outpt.write('node [shape=ellipse, fontcolor=black, style=filled,fillcolor=white];\n')
        for i in relations:
            outpt.write(''.join(i))

# End
    outpt.write('}')
    print '\n Output writen to: %s\n' % (flnm.replace('.bib','.dot'))


## Read file
f = open(flnm, 'r')
f.seek(0)
b = f.readlines()
f.close()


## Meat
# Identify records and its indexes
entry_starts = []
entry_ends = []
bibtype = []
for a in b:
    type_of_entry = p_type_of_entry.findall(a)
    if len(type_of_entry) > 0:
        entry_starts.append(b.index(a))
        entry_ends.append(b.index(a)-1)
        bibtype.append(type_of_entry)  # print bibtype to debug

entry_ends = entry_ends[1:]
entry_ends.append(len(b))

# Retrieving Information stored in each register and formating to .dot
rec = []
author = []
year = []
key = []
ke = []
auth = []
yea = []
relations = []
for e in range(len(entry_starts)):
    rec = ''.join(b[entry_starts[e]:entry_ends[e]]) 
    author = p_author.findall(rec)
    year = p_year.findall(rec)
    key = p_key.findall(rec)
    keyword = p_keyword.findall(rec)
    if (len(author) > 0 and len(year) > 0 and len(key) > 0):
        auth.append(author)
        yea.append(year)
        ke.append(key)
        key_to_keyword = key[0].replace(':','')
        if len(keyword) > 0:
            keywordes = keyword[0].split(',')
            for combi in keywordes:
                precombi = (combi.lstrip()).replace(' ','_')
                if len(re.compile('-').findall(precombi)) > 0:
                    prec = precombi.replace('-','')
                    relat = ([prec,' [label="',precombi,'"];\n',key_to_keyword, ' -> ', prec,' [dir=none];\n'])
                    relations.append(relat)
                else:
                    relat = ([key_to_keyword, ' -> ', precombi,' [dir=none];\n'])
                    relations.append(relat)
    elif (len(author) == 0 and len(year) == 0 and len(key) == 0):
        print '\nMeta information found between lines %d and %d, in %s\n' % (entry_starts[e],entry_ends[e],flnm)
    else:
        print '\nDiscarded record:'
        print '@',key, author, year
        print '...found between lines %d and %d, in %s' % (entry_starts[e],entry_ends[e],flnm)
        if len(key)==0:
            print '--bib key-- info is missing'
        if len(author)==0:
            print '--author-- info is missing'
        if len(year)==0:
            print '--year-- info is missing'



# entries_indexes = (entry_starts, entry_ends)
# for e in range(len(entry_starts)):
#     for c in b[entry_starts[e]:entry_ends[e]]:
#         author = p_author.findall(c)
#         if len(author) > 0:
#             auth.append(author)
#         year = p_year.findall(c)
#         if len(year) > 0:
#             yea.append(year)
#         keyword = p_keyword.findall(c)
#         key = p_key.findall(c)
#         if len(key) > 0:
#             key_to_keyword = key[0].replace(':','')
#         if len(keyword) > 0:
#             keywordes = keyword[0].split(',')
#             for combi in keywordes:
#                 precombi = (combi.lstrip()).replace(' ','_')
#                 if len(re.compile('-').findall(precombi)) > 0:
#                     prec = precombi.replace('-','')
#                     relat = ([prec,' [label="',precombi,'"];\n',key_to_keyword, ' -> ', prec,' [dir=none];\n'])
#                     relations.append(relat)
#                 else:
#                     relat = ([key_to_keyword, ' -> ', precombi,' [dir=none];\n'])
#                     relations.append(relat)

                
# Years line
y = []
y_almost = []
u_yea = unique_items(yea)
for i in u_yea:
    y_line = ''.join(i)
    y.append(y_line)
    Y = y
y.sort()
y_ready = ' -> '.join(y)

# Author; year
au = []
for i in auth:
    au_spl = p_auauthor.findall(''.join(i))
    if len(au_spl) == 0:
        au_spl = p_auauthor1.findall(''.join(i))
    au.append(au_spl)

long_labs = []
auth_yea = []
if len(au) == len(yea) & len(yea) == len(ke):
    for i in range(len(au)):
        kkk = ''.join(ke[i]).replace(':','')
        long_lab = kkk,' [label="','; '.join(au[i]),'"];\n'
        long_labs.append(long_lab)
        if opt_t == True:
            auth_ye = ('{rank=same; ',kkk,'; ',''.join(yea[i]),'; }\n')
        elif opt_t == False:
            auth_ye = (kkk,' -> ',''.join(yea[i]),'; \n')
        auth_yea.append(auth_ye)
    outputo()
else:
    print '\nUnknown error ! ! ! \n'
    print 'authors =', len(au)
    print 'years =', len(yea)
    print 'keys =', len(ke)
    print '\n'
    print bibtype
